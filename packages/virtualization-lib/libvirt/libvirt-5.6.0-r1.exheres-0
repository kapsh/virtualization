# Copyright 2009, 2011 Ingmar Vanhassel
# Copyright 2009-2017 Wulf C. Krueger <philantrop@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require systemd-service autotools [ supported_autoconf=[ 2.5 ] supported_automake=[ 1.16 ] ]
require option-renames [ renames=[ 'policykit polkit' ] ]
require openrc-service [ openrc_confd_files=[ "${FILES}"/openrc/confd ] ]

SUMMARY="A toolkit to interact with the virtualization capabilities of recent versions of Linux (and other OSes)"
DESCRIPTION="
Libvirt is a library for management of virtualized operating systems. Initially
supporting the Xen hypervisor, it provides a generic driver backend to allow
implementations for management of arbitrary hypervisors (QEMU, UML, etc).
The library aims to provide a long term stable API to isolate applications from
instability in the APIs of the underlying virtualization technology. The core
library API is written in C and a command line tool for shell scripting also
provided.
The library allows either full read-write access to the hypervisor, or for
unprivileged use a secure read-only channel (eg for monitoring).
"
HOMEPAGE="http://${PN}.org"
DOWNLOADS="${HOMEPAGE}/sources/${PNV}.tar.xz"

UPSTREAM_DOCUMENTATION="${HOMEPAGE}/docs.html [[ lang = en ]]"

LICENCES="LGPL-2.1"
SLOT="0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS="
    apparmor
    debug [[ description = [ Enable debugging output ] ]]
    iscsi [[ description = [ Enable the iSCSI storage driver ] ]]
    caps [[ description = [ Use libcap-ng to reduce libvirtd privileges ] ]]
    lvm [[ description = [ Enable the LVM storage driver ] ]]
    lxc [[ description = [ Enable support for LinuX Containers (LXC) ] requires = [ caps ] ]]
    network [[ description = [ Enable the virtual network driver ] ]]
    kvm [[ description = [ Enable the kvm/qemu interface ] ]]
    pcap [[ description = [ Enable networking optimisations using libpcap ] ]]
    pm-utils [[ description = [ use pm-utils for power management ] ]]
    polkit [[ description = [ Use polkit for UNIX socket access checks and password-less access (for the libvirt group) ] ]]
    raw [[ description = [ Enable the disk storage driver using parted ] ]]
    sasl [[ description = [ Use cyrus SASL for authentication ] ]]
    scsi [[ description = [ Enable the scsi storage driver ] ]]
    systemd [[ requires = [ -pm-utils ] ]]
    virtualbox [[ description = [ Enable the Virtualbox interface ] ]]
    vmware [[ description = [ Enable the VMware interface ] ]]
    xattr
    ( providers: libressl openssl ) [[ number-selected = exactly-one ]]
"

DEPENDENCIES="
    build:
        app-misc/mmv
        dev-libs/libxslt[>=1.1.24]
        dev-lang/perl:*
        dev-lang/python:*
        dev-util/intltool
        sys-devel/gettext[>=0.17]
        virtual/pkg-config
    build+run:
        dev-libs/gnutls[>=3.0]
        dev-libs/libtasn1[>=1.8]
        dev-libs/libgcrypt[>=1.4.4]
        dev-libs/libgpg-error[>=1.7]
        dev-libs/libxml2:2.0[>=2.6.0]
        dev-libs/nettle:=
        dev-libs/nss
        net-dns/libidn
        net-libs/libnl:3.0[>=3.0]
        net-libs/libssh2[>=1.3]
        net-misc/curl[>=7.18.0]
        sys-apps/dbus[>=1.2.12-r1]
        x11-libs/libpciaccess[>=0.11]
        apparmor? ( security/apparmor )
        iscsi? ( net-misc/open-iscsi[>=2.0.871] )
        kvm? ( dev-libs/yajl[>=2.0.3] )
        caps? ( sys-libs/libcap-ng[>=0.4.0] )
        lvm? ( sys-fs/lvm2[>=2.02.54] )
        lxc? (
            app-virtualization/lxc
            sys-fs/fuse:0[>=2.8.6]
        )
        pcap? ( dev-libs/libpcap[>=0.9.8] )
        pm-utils? ( sys-power/pm-utils )
        polkit? (
            dev-libs/expat[>=2.0.1]
            sys-auth/polkit:1
        )
        raw? ( sys-fs/parted[>=1.8.8][device-mapper] )
        sasl? ( net-libs/cyrus-sasl[>=2.1.26] )
        systemd? ( sys-apps/systemd[>=209] )
        xattr? ( sys-apps/attr )
        providers:libressl? ( dev-libs/libressl:= )
        providers:openssl? ( dev-libs/openssl )
    run:
        kvm? ( app-virtualization/qemu )
        network? (
            net-firewall/iptables
            sys-apps/iproute2
        )
    test:
        app-admin/augeas
    recommendation:
        network? ( net-dns/dnsmasq[>=2.49] ) [[ description = [
            Required for DHCP services with the virtual network driver
        ] ]]
    suggestion:
        net-firewall/ebtables [[ description = [
            Required for the network filter subsystem
        ] ]]
        net/netcat-openbsd [[ description = [
            Required for connecting to a remote hypervisor using ssh transport
        ] ]]
        sys-apps/dmidecode [[ description = [
            Required for gathering host information
        ] ]]
        polkit? ( group/libvirt ) [[ description = [
            Allow password-less access to libvirt for group members using polkit
        ] ]]
"

# 2.0.0: libvirt tries to create & use tons of sockets. The tests themselves are broken.
RESTRICT="test"

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --enable-nls
    --localstatedir=/var
    --with-blkid
    --with-chrdev-lock-files=/run/lock
    --with-curl
    --with-dbus
    --with-driver-modules
    --with-firewalld
    --with-html-dir=/usr/share/doc/${PNVR}
    --with-html-subdir=./
    --with-interface
    --with-libvirtd
    --with-macvtap
    --with-pciaccess
    --with-qemu-group=kvm
    --with-readline
    --with-remote
    --with-secrets
    --with-ssh2
    --with-storage-dir
    --with-storage-fs
    --with-sysctl
    --with-test
    --with-udev
    --with-virtualport
    --without-audit
    --without-bhyve
    --without-dtrace
    --without-esx
    --without-glusterfs
    --without-hal
    --without-hyperv
    --without-included-regex
    --without-libxl
    --without-netcf
    --without-numactl
    --without-numad
    --without-openvz
    --without-openwsman
    --without-phyp
    --without-sanlock
    --without-secdriver-selinux
    --without-selinux
    --without-selinux-mount
    --without-storage-gluster
    --without-storage-iscsi-direct
    --without-storage-rbd
    --without-storage-sheepdog
    --without-storage-zfs
    --without-vz
    # The wireshark dissector is broken with WS >= 2
    --without-wireshark-dissector
    --without-xenapi
)

DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=( debug )

DEFAULT_SRC_CONFIGURE_OPTION_WITHS=(
    "apparmor apparmor"
    "apparmor apparmor-mount"
    "apparmor apparmor-profiles"
    "apparmor secdriver-apparmor"
    "xattr attr"
    "iscsi storage-iscsi"
    "kvm qemu"
    "kvm yajl"
    "caps capng"
    "lvm storage-lvm"
    "lvm storage-mpath"
    lxc
    "lxc fuse"
    network
    "pcap libpcap"
    pm-utils
    polkit
    "raw storage-disk"
    sasl
    "scsi storage-scsi"
    "virtualbox vbox"
    vmware
)

DEFAULT_SRC_CONFIGURE_OPTIONS=(
    "systemd --with-init-script=systemd --with-init-script=none"
)

WORK=${WORKBASE}/${PNV/-rc*}

pkg_pretend() {
    if [[ -f "${ROOT}"/etc/tmpfiles.d/${PN}.conf ]] ; then
        ewarn "The configuration file /etc/tmpfiles.d/${PN}.conf has been moved to"
        ewarn "/usr/$(exhost --target)/lib/tmpfiles.d/${PN}.conf and can be safely removed after upgrade"
        ewarn "if you did not make any changes to it."
    fi
}

src_prepare() {
    AT_M4DIR=( m4 )

    # Set minimal, sensible defaults to get things going.
    edo sed \
        -e 's:#\(unix_sock_group =\).*:\1 "kvm":' \
        -e 's:#\(unix_sock_ro_perms =\).*:\1 "0550":' \
        -e 's:#\(unix_sock_rw_perms =\).*:\1 "0770":' \
        -i src/remote/libvirtd.conf

    # We don't use /etc/sysconfig but /etc/conf.d
    edo sed -e "/EnvironmentFile=-/s:\(/etc\)/sysconfig/\(.*\):\1/conf.d/\2.conf:" \
            -i src/remote/libvirtd.service.in \
            -i src/locking/virtlockd.service.in \
            -i tools/libvirt-guests.service.in

    autotools_src_prepare
}

src_test() {
    # There are quite a few network-related test issues but we're too strict
    # about that. None of those issues are fatal (the tests fail and count as
    # skipped eventually) and I won't fix those issues. If someone wants to, be
    # my guest.

    # This test would break the sandbox, needs a running libvirtd and doesn't
    # really add much in terms of QA because it only checks how libvirtd deals
    # with invalid config params.
    edo sed -i -e '/daemon-conf/d' tests/Makefile

    # These tests fail inside the sandbox for wanting to connect to several ports
    # on the local host or connecting to some host on the internet...
    edo sed -i -e 's:\(TESTS = \)virshtest$(EXEEXT) \(.*\):\1\2:' tests/Makefile
    edo sed -i -e 's:\(TESTS = \)nodeinfotest$(EXEEXT) \(.*\):\1\2:' tests/Makefile
    edo sed -i -e 's:commandtest$(EXEEXT) \\:\\:' tests/Makefile
    edo sed -i -e '/TESTS += test-bind/d' gnulib/tests/gnulib.mk
    edo sed -i -e '/check_PROGRAMS += test-bind/d' gnulib/tests/gnulib.mk
    edo sed -i -e '/TESTS += test-connect/d' gnulib/tests/gnulib.mk
    edo sed -i -e '/check_PROGRAMS += test-connect/d' gnulib/tests/gnulib.mk

    # This one's especially funny - it wants to "phone" home...
    edo sed -i -e '/TESTS += test-fwrite/d' gnulib/tests/gnulib.mk
    edo sed -i -e '/check_PROGRAMS += test-fwrite/d' gnulib/tests/gnulib.mk

    # Hard-codes some directories, owners and permissions which are irrelevant
    edo sed -i -e '/unix_sock_/d' src/remote/test_libvirtd.aug.in

    emake -j1 check
}

src_install() {
    local host=$(exhost --target)

    default

    insinto /usr/${host}/lib/tmpfiles.d
    hereins ${PN}.conf <<EOF
d /run/libvirt 0755 root root
d /run/libvirt/lxc 0755 root root
d /run/libvirt/network 0755 root root
d /run/libvirt/qemu 0755 root root
EOF

    if [[ -d ${IMAGE}/etc/sysconfig ]]; then
        dodir /etc/conf.d
        edo cp "${IMAGE}"/etc/sysconfig/* "${IMAGE}"/etc/conf.d/
        edo mmv "${IMAGE}/etc/conf.d/*" "${IMAGE}/etc/conf.d/#1.conf"
        edo rm "${IMAGE}"/etc/sysconfig/*
        edo rmdir "${IMAGE}"/etc/sysconfig
    fi

    keepdir \
        /usr/${host}/lib/libvirt/{connection-driver,drivers} \
        /var/cache/libvirt/{,qemu} \
        /var/lib/libvirt/{boot,dnsmasq,filesystems,images,lockd,lxc,network,qemu,swtpm} \
        /var/lib/libvirt/lockd/files \
        /var/lib/libvirt/iptables/{filter,nat} \
        /var/lib/libvirt/qemu/channel/target \
        /var/lib/libvirt/qemu/nvram \
        /var/log/libvirt/{lxc,qemu} \
        /var/log/swtpm/libvirt/qemu

    for dir in "${IMAGE}"/var/run/libvirt/{lxc,network,qemu/swtpm,qemu,lockd,} "${IMAGE}"/var/run; do
        [[ -d ${dir} ]] && edo rmdir "${dir}"
    done

    install_openrc_files
}

