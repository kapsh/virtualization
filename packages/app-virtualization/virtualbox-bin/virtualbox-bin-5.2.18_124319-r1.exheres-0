# Copyright 2008 Bernd Steinhauser <berniyh@exherbo.org>
# Copyright 2011, 2013 Ali Polatel <alip@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require setup-py [ import=distutils multibuild=true work=installer with_opt=true ] udev-rules

MY_PN=VirtualBox
MY_PV=${PV/_/-}
MY_PNV=${MY_PN}-${MY_PV}
EXT_PN=Oracle_VM_VirtualBox_Extension_Pack
# Oracle extension version number is sometimes different from the main version.
# In that case the following line need tweaking
EXT_PV=${MY_PV}
SUMMARY="x86 and AMD64/Intel64 virtualization software"
DESCRIPTION="

Binary distribution of VirtualBox, a general-purpose full virtualizer for x86
hardware targeted at server, desktop and embedded use.

VirtualBox supports a large number of guest operating systems including -- but
not limited to -- Windows (NT 4.0, 2000, XP, Server 2003, Vista, Windows 7),
DOS/Windows 3.x, Linux (2.4 and 2.6), Solaris and OpenSolaris, and OpenBSD.

Some of the features of VirtualBox are:
 * Modularity
 * Virtual machine descriptions in XML
 * Guest Additions for Windows and Linux
 * Shared folders
 * Virtual USB Controllers
 * Remote Desktop Protocol
 * USB over RDP
"
HOMEPAGE="http://www.virtualbox.org/"
DOWNLOADS="
listed-only:
    platform:amd64? ( http://download.virtualbox.org/virtualbox/${PV%_*}/${MY_PNV}-Linux_amd64.run )
    platform:x86? ( http://download.virtualbox.org/virtualbox/${PV%_*}/${MY_PNV}-Linux_x86.run )
    http://download.virtualbox.org/virtualbox/${PV%_*}/${EXT_PN}-${EXT_PV}.vbox-extpack -> ${EXT_PN}-${MY_PV}.tar.gz
"

UPSTREAM_DOCUMENTATION="
    ${HOMEPAGE}/wiki/Documentation [[ lang = en description = [ VirtualBox Documentation ] ]]
"
UPSTREAM_CHANGELOG="
    ${HOMEPAGE}/wiki/Changelog [[ lang = en ]]
"

LICENCES="GPL-2 VirtualBox"
SLOT="0"
PLATFORMS="~amd64 ~x86"

MYOPTIONS="
    platform:
        amd64
        x86
"

RESTRICT="strip"

DEPENDENCIES="
    run:
        !app-virtualization/virtualbox-ose
        dev-libs/glib:2
        group/usb
        group/vboxusers
        media-libs/SDL:0[>=1.2.7]
        media-libs/fontconfig
        media-libs/freetype:2
        media-libs/libpng:*
        x11-libs/libICE
        x11-libs/libSM
        x11-libs/libX11
        x11-libs/libXau
        x11-libs/libXcursor
        x11-libs/libXdmcp
        x11-libs/libXext
        x11-libs/libXfixes
        x11-libs/libXi
        x11-libs/libXinerama
        x11-libs/libXmu
        x11-libs/libXrandr
        x11-libs/libXrender
        x11-libs/libXt
        x11-libs/libxcb
"

# needed for python installer and vbox.cfg
export VBOX_INSTALL_PATH=/opt/${MY_PN}
export VBOX_VERSION=${PV%_*}

pkg_setup() {
    exdirectory --allow /opt
    exdirectory --allow /etc/modules-load.d
}

src_unpack() {
    edo cp "${FETCHEDDIR}"/${MY_PNV}-Linux_${PLATFORM}.run "${WORKBASE}"
    edo chmod u+x "${WORKBASE}"/${MY_PNV}-Linux_${PLATFORM}.run
    edo sh "${WORKBASE}"/${MY_PNV}-Linux_${PLATFORM}.run --noexec --target "${WORK}"
    edo mkdir "${WORK}"/opt
    edo cd "${WORK}"/opt
    edo mv "${WORK}"/${MY_PN}.tar.bz2 "${WORK}"/opt
    unpack ./${MY_PN}.tar.bz2
    edo rm ${MY_PN}.tar.bz2

    # Extension Pack
    edo mkdir "${WORK}"/${EXT_PN}
    edo pushd "${WORK}"/${EXT_PN}
    unpack ${EXT_PN}-${MY_PV}.tar.gz
    edo popd

    option python && easy-multibuild_src_unpack
}

unpack_one_multibuild() {
    edo cp -a "${WORK}"/opt/sdk/installer .
}

src_prepare() {
    # Some things should not be installed to /opt, but to the dirs we should
    # use for them, so we'll move them to another place.
    edo mkdir -p "${WORK}"/{doc,apps,icons}
    edo mv "${WORK}"/opt/virtualbox.desktop "${WORK}"/apps
    edo mv "${WORK}"/opt/VBox.png "${WORK}"/icons
    edo mv "${WORK}"/opt/UserManual.pdf "${WORK}"/doc
    edo mv "${WORK}"/opt/src "${WORK}"

    # Remove unwanted python libs
    edo rm -r "${WORK}"/opt/sdk/installer/
    edo rm "${WORK}"/opt/VBoxPython2_[456].so
    if ! optionq python_abis:2.7 || ! option python; then
      edo rm "${WORK}"/opt/VBoxPython2_7.so
    fi
    if [[ "$OPTIONS" != *python_abis:3* ]] || ! option python; then
      edo rm "${WORK}"/opt/VBoxPython.so
    fi

    option python && setup-py_src_prepare
}

prepare_one_multibuild() {
    edo mv vboxapisetup.py setup.py
}

src_configure() {
    option python && setup-py_src_configure
}

src_compile() {
    option python && setup-py_src_compile
}

src_install() {
    insinto /etc/vbox
    hereins vbox.cfg <<EOF
# VirtualBox installation directory
INSTALL_DIR='${VBOX_INSTALL_PATH}'
# VirtualBox version
INSTALL_VER='${VBOX_VERSION}'
EOF

    insinto /usr/share/applications/
    doins "${WORK}"/apps/virtualbox.desktop

    insinto /usr/share/pixmaps/
    newins "${WORK}"/icons/VBox.png virtualbox.png

    dodoc "${WORK}"/doc/UserManual.pdf

    insinto /usr/src/${PNV}
    doins -r "${WORK}"/src/*

    dodir /opt/${MY_PN} /usr/$(exhost --target)/bin
    edo mv "${WORK}"/opt/* "${IMAGE}"/opt/${MY_PN}
    for links in {VBoxHeadless,VBoxManage,VBoxSDL,VBoxVRDP,VirtualBox,vboxwebsrv,VBoxBalloonCtrl,VBoxAutostart}; do
        dosym /opt/${MY_PN}/VBox.sh /usr/$(exhost --target)/bin/${links}
    done
    dosym /opt/${MY_PN}/rdesktop-vrdp /usr/$(exhost --target)/bin/rdesktop-vrdp

    # Install Extension Pack
    edo pushd "${WORK}"/${EXT_PN}
    insinto /opt/${MY_PN}/ExtensionPacks/${EXT_PN}
    doins ExtPack* PXE-Intel.rom
    if option platform:amd64; then
        doins -r linux.amd64
    elif option platform:x86; then
        doins -r linux.x86
    else
        die "unsupported platform"
    fi
    edo popd

    # VirtualBox needs to be setuid
    edo chmod 4711 "${IMAGE}"/opt/${MY_PN}/{VirtualBox,VBoxManage,VBoxHeadless}
    edo chmod 4711 "${IMAGE}"/opt/${MY_PN}/{VBoxNetAdpCtl,VBoxNetDHCP}

    insinto "${UDEVRULESDIR}"
    hereins 10-vboxdrv.rules <<EOF
KERNEL=="vboxdrv", NAME="vboxdrv", OWNER="root", GROUP="vboxusers", MODE="0660"
KERNEL=="vboxnetctl", NAME="vboxnetctl", OWNER="root", GROUP="vboxusers", MODE="0660"
SUBSYSTEM=="usb_device", ACTION=="add", RUN+="/opt/${MY_PN}/VBoxCreateUSBNode.sh \$major \$minor \$attr{bDeviceClass} usb"
SUBSYSTEM=="usb", ACTION=="add", ENV{DEVTYPE}=="usb_device", RUN+="/opt/${MY_PN}/VBoxCreateUSBNode.sh \$major \$minor \$attr{bDeviceClass} usb"
SUBSYSTEM=="usb_device", ACTION=="remove", RUN+="/opt/${MY_PN}/VBoxCreateUSBNode.sh --remove \$major \$minor"
SUBSYSTEM=="usb", ACTION=="remove", ENV{DEVTYPE}=="usb_device", RUN+="/opt/${MY_PN}/VBoxCreateUSBNode.sh --remove \$major \$minor"
EOF

    insinto /etc/modules-load.d
    hereins ${PN}.conf <<EOF
vboxdrv
vboxnetflt
vboxnetadp
vboxpci
EOF

    # Fix for 2.2.0
    for symlink in VBox{DDU,REM,RT,VMM,XPCOM}.so; do
        dosym /opt/${MY_PN}/${symlink} /opt/${MY_PN}/components
    done

    option python && setup-py_src_install
}

src_test() {
    option python && setup-py_src_test
}

pkg_postinst() {
    ewarn "This exheres does not build the kernel modules for VirtualBox, you will have to do this"
    ewarn "on your own. The source code has been installed to /usr/src/${PNV}"

    if [[ -f /etc/udev/rules.d/60-virtualbox.rules ]]; then
        ewarn "Old rules file /etc/udev/rules.d/60-virtualbox.rules found."
        ewarn "You should remove this file."
    fi

    elog "In order to expose USB devices to VirtualBox guests, your user must be a member of"
    elog "the 'usb' group."
}
