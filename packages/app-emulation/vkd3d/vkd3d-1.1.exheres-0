# Copyright 2018 Timo Gurr <tgurr@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

SUMMARY="The Direct3D 12 to Vulkan translation library"
DESCRIPTION="
Vkd3d is a 3D graphics library built on top of Vulkan. It has an API very similar, but not
identical, to Direct3D 12.
"
HOMEPAGE="https://source.winehq.org/git/vkd3d.git"
DOWNLOADS="https://dl.winehq.org/vkd3d/source/${PNV}.tar.xz"

LICENCES="LGPL-2.1"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS="X"

DEPENDENCIES="
    build:
        sys-libs/spirv-headers
        sys-libs/vulkan-headers
        virtual/pkg-config
    build+run:
        sys-libs/vulkan-loader
        X? (
            x11-utils/xcb-util
            x11-utils/xcb-util-keysyms
        )
"

DEFAULT_SRC_PREPARE_PATCHES=(
    "${FILES}"/${PN}-1.1-fix-build.patch
)

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --disable-demos
    --disable-static
    --without-spirv-tools
)
DEFAULT_SRC_CONFIGURE_OPTION_WITHS=(
    'X xcb'
)

