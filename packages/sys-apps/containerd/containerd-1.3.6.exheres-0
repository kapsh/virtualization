# Copyright 2016 Marc-Antoine Perennou <marc-antoine.perennou@clever-cloud.com>
# Copyright 2017 Arnaud Lefebvre <arnaud.lefebvre@clever-cloud.com>
# Distributed under the terms of the GNU General Public License v2

SYSTEMD_SERVICE="containerd.service"

require systemd-service [ systemd_files=[ ${SYSTEMD_SERVICE} ] ] github [ tag=v${PV} ]

SUMMARY="A daemon to control runC"
DESCRIPTION="
containerd is a daemon to control runC, built for performance and density. containerd
leverages runC's advanced features such as seccomp and user namespace support as well as
checkpoint and restore for cloning and live migration of containers.
"
HOMEPAGE+=" https://containerd.io"

LICENCES="Apache-2.0"
SLOT="0"
PLATFORMS="~amd64 ~armv7 ~armv8"
MYOPTIONS=""

DEPENDENCIES="
    build:
        dev-lang/go[>=1.10]
    build+run:
        sys-libs/libseccomp
        sys-fs/btrfs-progs
    run:
        sys-apps/runc[>=1.0.0-rc9]
"

# Tests try to run golint and install stuff into /usr/local
# Do not strip Go binaries
RESTRICT="strip test"

# git rev-list -1 v1.3.4 - needed to set REVISION below to avoid calling git
# tools in Makefile.
COMMIT="814b7956fafc7a0980ea07e950f983d0837e5578"

GOWORK=src/github.com/${PN}

containerd_make() {
    # make test also make bins again
    # so we need the same exports
    pushd ${GOWORK}/${PN}
    LDFLAGS= GOPATH=$(pwd) emake "$@" VERSION=v${PV} REVISION=${COMMIT}
    popd
}

src_prepare() {
    default

    edo mkdir -p ${GOWORK}
    edo ln -s ${WORK} ${GOWORK}/${PN}
    edo sed \
        -e 's:local/bin:host/bin:g' \
        -i ${SYSTEMD_SERVICE}
}

src_compile() {
    containerd_make
}

src_test() {
    containerd_make test
}

src_install() {
    dobin bin/*
    install_systemd_files
}

